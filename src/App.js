import React from 'react';
import './App.css';
import LayoutApp from './components/layout/Layout';
import CurrencyReport from './containers/currencyReport/CurrencyReport';
function App() {
  return (
    <LayoutApp>
      <CurrencyReport/>
    </LayoutApp>
  );
}
export default App;
