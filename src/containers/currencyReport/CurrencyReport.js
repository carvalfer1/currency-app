import React, { useState, useEffect } from "react";
import { DatePicker, notification } from "antd";
import { Area } from "@ant-design/charts";
import moment from "moment";
import InputGroup from "../../components/inputGroup/InputGroup";
import CardContainer from "./cardContainer/CardContainer";
import axios from "axios";
import "./CurrencyReport.scss";
import { buildUrl, calculate } from "./utils";
const { RangePicker } = DatePicker;
const dateFormat = "DD/MM/YYYY";

const CurrencyReport = () => {
  const [data, setData] = useState([]);
  const [config, setConfig] = useState(null);
  const [summary, setSummary] = useState(null);
  useEffect(() => {
    if (data.length > 0) {
      const dataFormated = data.map((item) => {
        const obj = {
          ...item,
          Valor: parseFloat(item.Valor.replace(",", ".")),
        };
        return obj;
      });
      debugger;
      setConfig({
        title: {
          visible: false,
          text: "Tasa del dolar en la fecha seleccionada",
        },
        data: dataFormated,
        xField: "Fecha",
        yField: "Valor",
        xAxis: {
          type: "dateTime",
          tickCount: 6,
        },
        yAxis: {
          tickCount: 8,
        },
        padding: "auto",
      });
      setSummary(calculate(dataFormated));
    }
  }, [data]);

  // Para no permitir fechas futuras o fechas mayores al día de hoy.
  const disabledDate = (current) => {
    return current > moment();
  };

  // Llamar al rest
  const handleCallApi = (selectedDate) => {
    if (selectedDate.length > 1) {
      if (selectedDate[0] !== null && selectedDate[1] !== null) {
        console.log("Call Api");
        callApi(selectedDate);
      }
    }
  };

  const callApi = (selectedDate) => {
    axios
      .get(buildUrl(selectedDate))
      .then((response) => {
        setData(response.data.Dolares);
      })
      .catch((error) => {
        console.log(error);
        notification["error"]({
          message: "¡Ha ocurrido un error!",
          description:
            "Hemos dejado un mensaje con el error en la consola del navegador, intenta mas tarde o contula con tu administrador",
        });
      });
  };

  console.log(config);

  return (
    <div className={"currency-report"}>
      <InputGroup span="Selecciona rango de fecha para graficar">
        <RangePicker
          disabledDate={disabledDate}
          format={dateFormat}
          onCalendarChange={(value) => {
            console.log("asd");
            handleCallApi(value);
          }}
        />
      </InputGroup>

      {config && <Area {...config} />}

      {summary && <CardContainer {...summary} />}
    </div>
  );
};

export default CurrencyReport;
