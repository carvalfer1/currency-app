import React from "react";
import { Card, Col, Row } from "antd";
import "./CardContainer.scss";

const CardContainer = (props) => {
  return (
    <div className={"card-container"}>
      <Row gutter={16}>
        <Col span={8} xs={24} sm={24} md={8} lg={8} xl={8}>
          <Card title="Promedio">
            {props.average}
          </Card>
        </Col>
        <Col span={8} xs={24} sm={24} md={8} lg={8} xl={8}>
          <Card title="Valor Máximo">
            {props.max}
          </Card>
        </Col>
        <Col span={8} xs={24} sm={24} md={8} lg={8} xl={8}>
          <Card title="Valor Mínimo">
            {props.min}
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default CardContainer;
