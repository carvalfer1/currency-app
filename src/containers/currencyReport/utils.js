import {
  APIKEY,
  APIDOMAIN,
  REQUESTFORMAT,
  DATEFORMAT,
} from "../../config/constants";

export const buildUrl = (dates) => {
  const startDateArray = dates[0].format(DATEFORMAT).split("/");
  const endDateArray = dates[1].format(DATEFORMAT).split("/");
  let apiEndPoint = `${APIDOMAIN}`;
  apiEndPoint =
    apiEndPoint +
    `/${startDateArray[2]}/${startDateArray[1]}/dias_i/${startDateArray[0]}`;
  apiEndPoint =
    apiEndPoint +
    `/${endDateArray[2]}/${endDateArray[1]}/dias_f/${endDateArray[0]}`;
  apiEndPoint = apiEndPoint + `?apikey=${APIKEY}`;
  apiEndPoint = apiEndPoint + `&formato=${REQUESTFORMAT}`;
  return apiEndPoint;
};

export const calculate = (data) => {
  let send = null;
  const result = data.reduce((acum, current, currentIndex) => {
    if (currentIndex > 1) {
      send = {
        sum: current.Valor + acum.sum,
        max: current.Valor > acum.max ? current.Valor : acum.max,
        min: current.Valor < acum.min ? current.Valor : acum.min,
      };
    } else {
      send = {
        sum: current.Valor + acum.Valor,
        max: current.Valor > acum.Valor ? current.Valor : acum.Valor,
        min: current.Valor < acum.Valor ? current.Valor : acum.Valor,
      };
    }
    return send;
  });
  return { ...result, average: (result.sum / data.length).toFixed(2) };
};
