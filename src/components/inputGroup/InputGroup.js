import React from "react";
import "./InputGroup.scss";

const InputGroup = (props) => {
  return (
    <div className="input-group">
      <span>{props.span}</span>
      <div>{props.children}</div>
    </div>
  );
};

export default InputGroup;
