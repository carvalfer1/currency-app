import React from "react";
import Header from "./header/Header";
import Footer from "./footer/Footer";
import Body from "./body/Body";

const Layout = (props) => {
  return (
    <div>
      <Header />
      <Body children={props.children}/>
      <Footer />
    </div>
  );
};

export default Layout;
