import React from "react";
import "./Body.scss";

const Body = (props) => {
  return (
    <div className="body-app">
      <div>{props.children}</div>
    </div>
  );
};

export default Body;
